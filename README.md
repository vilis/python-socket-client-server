# Python Socket Client-Server
This project is a Python-based client-server application using low-level socket programming. It allows users to establish a client-server connection, send and receive data, and perform a variety of operations.
Users data is stored in JSON file.

## Commands
Below are the supported commands and their descriptions:

- create - Creates new user account.

- login - User login.

- send - Sends a message to the specified user on the server. Note that this operation can only be done when the user is logged in.

- uptime - Provides the server uptime in seconds. This command will give you the time elapsed since the server was created.

- stop - Stops the server and ends the client-server connection. After executing this command, the connection will be terminated.

- info - Provides information about the server version.

- help - Provides a list of all available commands that can be executed.

Additionally, users must be logged in to execute some commands. If a user attempts to perform an operation without logging in.

## Installation
1. Clone the repository:

<code>git clone https://gitlab.com/vilis/python-socket-client-server.git </code>

2. Move into the project directory:

<code> cd python-socket-client-server </code>

3. Run the server script:

<code>python3 server.py</code>

4. In a new terminal, run the client script:

<code> python client.py </code>

You will then be able to execute the above commands from the client terminal.

## License
Distributed under the MIT License.


