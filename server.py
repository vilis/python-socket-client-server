import socket
import json
import time


class Server():
    def __init__(self):
        self.HOST = "127.0.0.1"
        self.PORT = 65432
        self.version = "1.0.0"
        self.server_time_start = 0
        self.listening = self.start_listening()

    def start_listening(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.HOST, self.PORT))
        s.listen()
        self.start_servertime()
        return s

    def start_servertime(self):
        self.server_time_start = time.perf_counter()

    def server_uptime(self):
        return time.perf_counter() - self.server_time_start

    def send_info(self, connection, message, data):
        text = json.dumps({f"{message}": f"{data}"})
        connection.send(text.encode('ascii'))

    def send_and_receive_data(self, connection, data):
        connection.send(data.encode('ascii'))
        return connection.recv(1024).decode('ascii')

    def stop_server(self, connection, server):
        connection.close()
        server.listening.close()

    def login(self, connection):
        login_data = []
        login = server.send_and_receive_data(connection, 'Login: ')
        print(f"user login is {login}")
        login_data.append(login)
        password = server.send_and_receive_data(
            connection, 'Password: ')
        print(f"user password is {password}")
        login_data.append(password)

        with open('data.json',) as f:
            data = json.load(f)
            result = [user for user in data['users'] if user['login']
                      == login_data[0] and user['password'] == login_data[1]]
            print(result)
            if result:
                print("User has been found")
                return True, login_data
            else:
                print("User not found!")
                login_data.clear()
                return False

    def send(self, connection):
        username = server.send_and_receive_data(
            connection, "Enter user name: ")
        message = server.send_and_receive_data(
            connection, "Enter message: ")
        with open('data.json', 'r+') as f:
            data = json.load(f)
            result = [user for user in data['users']
                      if user['login'] == username]

            if result:
                if result[0]['type'] == "user":
                    if len(result[0]['messages']) < 5:
                        print(result[0]["messages"])
                        server.append_data_to_json(
                            result[0], connection, data, message)

                    else:
                        print("user has more than 5 messages")
                        print(result[0]["messages"])
                        del result[0]["messages"][0]
                        server.append_data_to_json(
                            result[0], connection, data, message)

                else:
                    print(result[0]["messages"])
                    server.append_data_to_json(
                        result[0], connection, data, message)

            else:
                connection.send("User doesn't exist".encode('ascii'))

    def create(self, connection):
        username = server.send_and_receive_data(
            connection, "Enter new user login: ")
        password = server.send_and_receive_data(
            connection, "Enter new user password: ")

        with open('data.json',) as f:
            data = json.load(f)
            result = [user for user in data['users']
                      if user['login'] == username]
            if result:
                connection.send("User exists".encode('ascii'))

            else:
                new_user = User(username, password)
                new_user.create_user()
                connection.send(
                    "User successfully created!".encode('ascii'))

    def help(self):
        data = json.dumps({
            "uptime": "returns server's life time",
            "info": "returns server version number",
            "help": "available commands",
            "stop": "stops client and server"
        }, indent=2)
        return data

    def append_data_to_json(self, user, connection, data, message):
        user["messages"].append(message)
        connection.send("Message has been sent".encode('ascii'))

        with open('data.json', 'w') as f:
            json.dump(data, f, indent=4)
        # for messages in user['messages']:
        #     print(messages)


class User():
    def __init__(self, login, password):
        self.user_login = login
        self.user_password = password

    def create_user(self, type="user"):
        data = {"login": self.user_login,
                "password": self.user_password,
                "type": type,
                "messages": []}

        with open('data.json', 'r+') as file:
            file_data = json.load(file)
            file_data["users"].append(data)
            file.seek(0)
            with open('data.json', 'w') as f:
                json.dump(file_data, f, indent=4)

    #not necessary
    def all_users(self):
        with open('data.json',) as f:
            # Reading from json file
            data = json.load(f)
            for i in data['users']:
                print(i)


if __name__ == '__main__':

    server = Server()

    while True:
        logged = False
        connection, address = server.listening.accept()
        print(f"connected to {address}")

        while True:
            command = connection.recv(1024).decode('ascii')
            print(f"command is {command}")
            if command == "login":
                if logged:
                    connection.send(
                        "You are already logged in\n".encode('ascii'))
                    continue

                logged = server.login(connection)
                if logged:
                    connection.send("Successfuly logged in\n".encode('ascii'))
                    user = User(logged[1][0], logged[1][1])
                if not logged:
                    connection.send("Wrong user or password\n".encode('ascii'))

                continue

            elif command == "create":
                server.create(connection)
                continue

            if command == "send" and logged:
                server.send(connection)

            elif logged is False:
                connection.send("You are not logged in\n".encode('ascii'))

            if command == "uptime":
                server.send_info(
                    connection, "uptime", f"{server.server_uptime():0.2f} seconds")

            if command == "stop":
                server.send_info(connection, "connection", f"{command}ped")
                print("connection ended on client")
                server.stop_server(connection, server)
                break

            if command == "info":
                server.send_info(connection, "version", f"{server.version}")

            if command == "help":
                connection.send(server.help().encode('ascii'))

        break
