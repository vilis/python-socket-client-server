import socket

HOST = "127.0.0.1"
PORT = 65432


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

command = ""


def send_and_receive_data(command):
    s.send(command.encode('ascii'))
    return s.recv(1024).decode('ascii')

print("Type 'login' to login or create new user by typing 'create'")

print("Type 'login' to login or create new user by typing 'create'")

while True:
    command = input("What do you want to do? Use help for command list\n")

    if command == "login":
        login = input(send_and_receive_data(command))
        password = input(send_and_receive_data(login))
        print(send_and_receive_data(password))
        continue

    if command == "create":
        username = input(send_and_receive_data(command))
        password = input(send_and_receive_data(username))
        data = send_and_receive_data(password)
        print(data)

    if command == "send":
        username = input(send_and_receive_data(command))
        message = input(send_and_receive_data(username))
        print(send_and_receive_data(message))
        continue

    if command == "uptime":
        print(send_and_receive_data(command))

    if command == "stop":
        print(send_and_receive_data(command))
        s.close()
        break

    if command == "info":
        print(send_and_receive_data(command))

    if command == "help":
        print(send_and_receive_data(command))

    else:
        print("Command not found")
